function generateCustomerList(customers) {
    let rows = "";
    for(let i = 0; i < customers.length; i++) {
        rows += `
            ${generateCustomerRowHeading(customers[i])}
            ${generateCustomerRowElement(
                'Kliendi esindaja:', customers[i].contactName != null ? customers[i].contactName : 'info puudub',
                'email:', customers[i].email !=null ? (customers[i].email) : 'info puudub'
            )}
            ${generateCustomerRowElement(
                'Telefon:', customers[i].phoneNr > 0 ? (customers[i].phoneNr) : 'info puudub',
                'Asukoht:', customers[i].location != null ? (customers[i].location) : 'info puudub'
            )}
            
            ${generateCustomerRowButtons(customers[i])}
        `;
    }

    document.getElementById("customerList").innerHTML = /*html*/`
        <div class="row justify-content-center">
            <div class="col-lg-10">
                ${rows}
                <div class="row">
                    <div class="col-12" style="padding: 5px;">
                        <button class="btn btn-success btn-block" onClick="openCustomerModalForInsert()">Lisa Tellija</button>
                    </div>
                </div>
            </div>
        </div>
    `;
}

function generateCustomerRowHeading(customers) {
    return /*html*/`
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <strong style="font-size: 28px;">${customers.customerName}</strong>
            </div>
        </div>

    `;
}

function generateCustomerRowElement(cell1Content, cell2Content, cell3Content, cell4Content) {
    return /*html*/`
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    ${generateCustomerRowTitleCell(cell1Content)}
                    ${generateCustomerRowTitleCell(cell2Content)}
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    ${generateCustomerRowTitleCell(cell3Content)}
                    ${generateCustomerRowTitleCell(cell4Content)}
                </div>
            </div>
        </div>
    `;
}

function generateCustomerRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateCustomerRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                ${text}
            </div>
        </div>
    `;
}

function generateCustomerRowButtons(customers) {
    return /*html*/`
        <div class="row justify-content-center">
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-primary btn-block" onClick="openCustomerModalForUpdate(${customers.customerNr})">Muuda</button>
            </div>
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-danger btn-block" onClick="removeCustomer(${customers.customerNr})">Kustuta</button>
            </div>
            <div class="col-sm-3" style="padding: 10px;">
            <button class="btn btn-success btn-block" onClick="location.href='priceoffer.html?customerNr=${customers.customerNr}';">Pakkumised</button>
            </div>
            
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;
}


function openCustomerModal() {
    $("#customerModal").modal('show');
}

function closeCustomerModal() {
    $("#customerModal").modal('hide');
}

function clearError() {
    document.getElementById("errorPanel").innerHTML = "";
    document.getElementById("errorPanel").style.display = "none";
}

function showError(message) {
    document.getElementById("errorPanel").innerHTML = message;
    document.getElementById("errorPanel").style.display = "block";
}

function clearCustomerModal() {
   document.getElementById("customerNr").value = null;
    document.getElementById("customerName").value = null;
    document.getElementById("contactName").value = null;
    document.getElementById("email").value = null;
    document.getElementById("phoneNr").value = null;
    document.getElementById("location").value = null;

}

function fillCustomerModal(customers) {
    document.getElementById("customerNr").value = customers.customerNr;
    document.getElementById("customerName").value = customers.customerName;
    document.getElementById("contactName").value = customers.contactName;
    document.getElementById("email").value = customers.email;
    document.getElementById("phoneNr").value = customers.phoneNr;
    document.getElementById("location").value = customers.location;

}

function openComponentModal() {
    $("#componentModal").modal('show');
}

function closeComponentModal() {
    $("#componentModal").modal('hide');
}
