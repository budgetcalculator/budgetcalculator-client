
function loadCustomers() {
    fetchCustomers().then(generateCustomerList);
}
//Muutmine
function openCustomerModalForUpdate(customerNr) {
    clearError();
    fetchCustomer(customerNr).then(customers => {
        openCustomerModal();
        clearCustomerModal();
        fillCustomerModal(customers);
    });
}

function openCustomerModalForInsert() {
    openCustomerModal();
    clearError();
    clearCustomerModal();
}



function saveCustomer() {
    let customer = getCustomerFromModal();
    if (validateCustomerModal(customer)) {
        postCustomer(customer)
            .then(() => {
                closeCustomerModal();
                loadCustomers();
            });
    }
}

function removeCustomer(customerNr) {
    if (confirm('Soovid kliendi kustutada?')) {
        deleteCustomer(customerNr).then(loadCustomers)
    }
}


function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
            loadCustomers();
        })
    }
}

function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}



function loadCustomerPriceOffers() {
    const urlParams = new URLSearchParams(window.location.search);
    const customerNr = urlParams.get('customerNr');
    if (customerNr > 0) {
        fetchCustomerPriceOffers(customerNr).then(generateOfferRows);
    } else {
        fetchPriceOffers().then(generateOfferRows);
    }
}
//Objekti pop aken
function openObjectModalForUpdate(priceOfferId) {
    clearError();
    fetchObject(priceOfferId).then(customers => {
        openObjectModal();
        clearObjectModal();
        fillObjectModal(customers);
    });
}

function openObjectModalForInsert() {
    openObjectModal();
    clearError();
    clearObjectModal();
}
//Lisa hinnapakkumine
async function addPriceOffer() {
    const urlParams = new URLSearchParams(window.location.search);
    const customerNr = urlParams.get('customerNr');
    await postPriceOffer(customerNr);
    loadCustomerPriceOffers();
}
//Tagasinupu funktsioonid
function getQueryParam(paramName) {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.get(paramName);
}

function getCustomerNrFromUrl() {
    const urlParams = new URLSearchParams(window.location.search);
    const customerNr = urlParams.get('customerNr');

    return customerNr;
}

function navigateToPriceOffers() {
    const customerNr = getCustomerNrFromUrl();
    document.location = `priceoffer.html?customerNr=${customerNr}`;
}

//savePriceOfferDetails
function savePriceOfferDetails() {
    let buildings = getObjectFromModal();
    postObject(buildings)
        .then(() => {
            closeObjectModal();
            loadObjects();
        });

}
async function loadObjects() {
    await getQueryParam('priceOfferId');
    if (getQueryParam('priceOfferId') > 0) {
        fetchPriceOfferId(getQueryParam('priceOfferId')).then(generateOfferRows);
    } else {
        fetchObjects().then(generateOfferRows);
    }
}

function saveComponent() {
    let component = getComponentFromModal();
    postComponent(component)
        .then(() => {
            closeComponentModal();
            loadPriceOfferComponents(priceOfferDetailsId).then(displayPriceOfferComponents);
        });
}

function openComponentModalForInsert() {
    openComponentModal();
    clearError();
    //clearComponentModal();
}
function saveWork(id) {
    let work = getWorksFromTable(id);
    let material = getMaterialFromTable(id);
    postWork(work)
    postMaterial(material)
        .then(() => {
            loadPriceOfferComponents(priceOfferDetailsId).then(displayPriceOfferComponents);
        });
}

async function addRow(componentId) {
    let material = addEmptyMaterialRow();
    let response = await postCell(material);
    let materialId = await response.text(); 
    let work = addEmptyWorkRow(componentId, materialId);
    await postRow(work);
    loadPriceOfferComponents(priceOfferDetailsId).then(displayPriceOfferComponents);
        
}
