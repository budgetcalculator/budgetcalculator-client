
let allComponents = [];

function fetchCustomers() {
    return fetch(
        `${API_URL}/companies/customers`,
        {
            method: 'GET'
        }
    )
        .then(checkResponse)
        .then(customers => customers.json());
}

function fetchCustomer(customerNr) {
    return fetch(
        `${API_URL}/companies/customers/${customerNr}`,
        {
            method: 'GET',
            // headers: {
            //     // 'Authorization': `Bearer ${getToken()}`
            // }
        }
    )
        .then(checkResponse).then(customers => customers.json());
}


function postCompany(company) {
    return fetch(
        `${API_URL}/customers`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(customer)
        }
    )
        .then(checkResponse);
}

function deleteCustomer(customerNr) {
    return fetch(
        `${API_URL}/companies/customers/${customerNr}`,
        {
            method: 'DELETE',
            // headers: {
            //     // 'Authorization': `Bearer ${getToken()}`
            // }
        }
    )
        .then(checkResponse);
}

function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
        .then(checkResponse)
        .then(session => session.json());
}

function checkResponse(response) {

    showMainContainer();
    return response;
}


//calc algab siit
function postCustomer(customers) {
    return fetch(
        `${API_URL}/companies/customers`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(customers)
        }
    )
        .then(checkResponse);
}

function fetchPriceOffers() {
    return fetch(
        `${API_URL}/companies/priceoffers`,
        {
            method: 'GET'
        }
    )
        .then(checkResponse)
        .then(priceOffers => priceOffers.json());
}

function fetchCustomerPriceOffers(customerNr) {
    let formData = new FormData();
    formData.append('customerNr', customerNr);

    return fetch(
        `${API_URL}/companies/priceOffers/search`,
        {
            method: 'POST',
            body: formData
        }
    )
        .then(checkResponse)
        .then(priceOffers => priceOffers.json());
}
//Price offer
function postPriceOffer(customerNr) {
    return fetch(
        `${API_URL}/companies/priceoffers`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ "customerNr": customerNr })
        }
    )
        .then(checkResponse);
}

//objektid pop
function postObject(buildings) {
    return fetch(
        `${API_URL}/companies/priceofferdetails`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(buildings)
        }
    )
        .then(checkResponse);
}
function fetchObjects() {
    return fetch(
        `${API_URL}/companies/priceofferdetails`,
        {
            method: 'GET'
        }
    )
        .then(checkResponse)
        .then(priceOfferDetails => priceOfferDetails.json());
}

function fetchPriceOfferId(priceOfferId) {
    let formData = new FormData();
    formData.append('priceOfferId', priceOfferId);

    return fetch(
        `${API_URL}/companies/priceofferdetails/search`,
        {
            method: 'POST',
            body: formData
        }
    )
        .then(checkResponse)
        .then(priceOfferDetails => priceOfferDetails.json());
}
function loadMaterials() {
    return fetch(
        `${API_URL}/companies/materials`,
        {
            method: 'GET'
        }
    )
        .then(checkResponse)
        .then(materials => materials.json());
}
function fetchPriceOfferDetailsId(priceOfferDetailsId) {
    let formData = new FormData();
    formData.append('priceOfferDetailsId', priceOfferDetailsId);

    return fetch(
        `${API_URL}/companies/priceofferdetails/search`,
        {
            method: 'POST',
            body: formData
        }
    )
        .then(checkResponse)
        .then(priceOfferDetails2 => priceOfferDetails2.json());
}
function fetchMaterials(id) {
    return fetch(
        `${API_URL}/companies/materials/${id}`,
        {
            method: 'GET',
            // headers: {
            //     // 'Authorization': `Bearer ${getToken()}`
            // }
        }
    )
        .then(checkResponse).then(materials => materials.json());
}
function loadComponents() {
    return fetch(
        `${API_URL}/companies/priceoffercomponent`,
        {
            method: 'GET'
        }
    )
        .then(checkResponse)
        .then(components => components.json());
}

async function loadPriceOfferComponents(priceOfferDetailId) {
    let formData = new FormData();
    formData.append("priceOfferDetailId", priceOfferDetailId);
    let response = await fetch(`${API_URL}/companies/priceoffercomponent/search`, {
        method: "POST",
        body: formData
    });
    checkResponse(response);
    allComponents = await response.json();
    console.log(allComponents);
}

function postComponent(component) {
    return fetch(
        `${API_URL}/companies/priceoffercomponent`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',

            },
            body: JSON.stringify(component)
        }
    )
        .then(checkResponse);
}
function postWork(work) {
    return fetch(
        `${API_URL}/companies/work`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',

            },
            body: JSON.stringify(work)
        }
    )
        .then(checkResponse);
}

function postMaterial(material) {
    return fetch(
        `${API_URL}/companies/materials`,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',

            },
            body: JSON.stringify(material)
        }
    )
        .then(checkResponse);
}
//add empty work
function postRow(work) {
    return fetch(
        `${API_URL}/companies/work`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(work)
        }
    )
        .then(checkResponse);
}

function postCell(material) {
    return fetch(
        `${API_URL}/companies/materials`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',

            },
            body: JSON.stringify(material)
        }
    );
}