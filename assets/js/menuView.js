
function generateTopMenu() {
    let logoutLink = "";
    if (!isEmpty(getUsername())) {
        logoutLink = /*html*/`${getUsername()} | <a href="javascript:logoutUser()" style="color: #fff; font-weight: bold;">logi välja</a>`;
    }

    document.getElementById("topMenuContainer").innerHTML = /*html*/`
        <div style="padding: 0; margin: 0; color: white; font-style: italic; background:darkblue;">
            <div class="row">
                <div class="col-6">
                    <strong>Budgetcalculator 1.0</strong>
                </div>
                <div class="col-6" style="text-align: right; font-style: normal;">
                    
                </div>
            </div>
        </div>
    `;
}

function showLoginContainer() {
    document.getElementById("loginContainer").style.display = "block";
    document.getElementById("mainContainer").style.display = "none";
}

function showMainContainer() {
    if (document.getElementById("loginContainer")) {
        document.getElementById("loginContainer").style.display = "none";
    }
    if (document.getElementById("mainContainer")) {
        document.getElementById("mainContainer").style.display = "block";
    }
}

// function changeView() {
//     if(document.getElementById("viewSelect").value === "CHART") {
//         showCompanyChart();
//     } else {
//         showCompanyList();
//     }
// }

function showCustomerList() {
    document.getElementById("customerList").style.display = "block";
    document.getElementById("customerChart").style.display = "none";
    loadCompanies();
}

function showCustomerChart() {
    document.getElementById("customerList").style.display = "none";
    document.getElementById("customerChart").style.display = "block";
    loadChart();
}
