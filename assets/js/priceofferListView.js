

function generateCustomerRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="background: #EAEDED; border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                <strong>${text}</strong>
            </div>
        </div>
    `;
}

function generateCustomerRowTitleCell(text) {
    return /*html*/`
        <div class="col-sm-6" style="padding: 5px;">
            <div style="border-top: 1px solid #CCD1D1; border-bottom: 1px solid #CCD1D1; margin: 2px; padding: 10px; text-align: center;">
                ${text}
            </div>
        </div>
    `;
}

function generateOfferRowButtons(customers) {
    return /*html*/`
        <div class="row justify-content-center">
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-primary btn-block" onClick="openCustomerModalForUpdate(${customers.customerNr})">Lisa/Muuda tööd</button>
            </div>
            <div class="col-sm-3" style="padding: 10px;">
                <button class="btn btn-danger btn-block" onClick="removeCustomer(${customers.customerNr})">Kustuta</button>
            </div>

            
        </div>
        <div class="row">
            <div class="col-12" style="padding: 10px;">
                <hr/>
            </div>
        </div>
    `;
}

// function generateImageElement(logo) {
//     if (!isEmpty(logo)) {
//         return /*html*/`<img src="${logo}" width="150" />`;
//     } else {
//         return "[PILT PUUDUB]";
//     }
// }

function openCustomerModal() {
    $("#customerModal").modal('show');
}

function closeCustomerModal() {
    $("#customerModal").modal('hide');
}

function clearError() {
    document.getElementById("errorPanel").innerHTML = "";
    document.getElementById("errorPanel").style.display = "none";
}

function showError(message) {
    document.getElementById("errorPanel").innerHTML = message;
    document.getElementById("errorPanel").style.display = "block";
}

function clearCustomerModal() {
    document.getElementById("customerNr").value = null;
    document.getElementById("customerName").value = null;
    document.getElementById("contactName").value = null;
    document.getElementById("email").value = null;
    document.getElementById("phoneNr").value = null;
    document.getElementById("location").value = null;

}

function fillCustomerModal(customers) {
    document.getElementById("customerNr").value = customers.customerNr;
    document.getElementById("customerName").value = customers.customerName;
    document.getElementById("contactName").value = customers.contactName;
    document.getElementById("email").value = customers.email;
    document.getElementById("phoneNr").value = customers.phoneNr;
    document.getElementById("location").value = customers.location;

}


async function generateOfferRows(priceOffers) {
    let result = await fetchPriceOffers();
    let priceOffersSuva = '';
    for (let i = 0; i < priceOffers.length; i++) {
        priceOffersSuva = priceOffersSuva + `
                <tr>
                <td>${priceOffers[i].id}</td>
                <td>${priceOffers[i].customerNr}</td>
                <td>${priceOffers[i].date}</td>
                <td>${priceOffers[i].latestUpdate}</td>
                <td>
                <button type="button" class="btn btn-primary" onClick="location.href='priceofferdetails.html?priceOfferId=${priceOffers[i].id}';">Muuda</button>
                <button type="button" class="btn btn-danger" onclick="handleCompanyDelete(${priceOffers[i].id})">Kustuta</button>
                
                </td>
                </tr>
                `;
    }
    document.querySelector("#priceOffersSuva").innerHTML = priceOffersSuva;
}
