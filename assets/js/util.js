
// Validation functions

function validateCustomerModal(customer) {
    if(isEmpty(customer.customerName)) {
        showError("Ettevõtte nimi on kohustuslik!");
        return false;
    }
    if(isEmpty(customer.contactName) ) {
        showError("Esindaja nimi on kohustuslik!");
        return false;
    }
    if(isEmpty(customer.email)) {
        showError("Email on kohustuslik");
        return false;
    }
    if(isEmpty(customer.phoneNr) ) {
        showError("Telefoni number on kohustuslik");
        return false;
    }
    if(isEmpty(customer.location) ) {
        showError("Töö asukoht on kohustuslik");
        return false;
    }

    
    return true;
}

function validateCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}

function isInteger(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+$/;
    return regex.test(text);
}

function isDecimal(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+(\.\d+)?$/;
    return regex.test(text);
}

function isEmpty(text) {
     return (!text || 0 === text.length);
}

// DOM retrieval functions

function getFileInput() {
    return document.getElementById("file");
}

function getCustomerFromModal() {
    return {
        "customerNr": document.getElementById("customerNr").value, 
        "customerName": document.getElementById("customerName").value,
        "contactName": document.getElementById("contactName").value,
        "email": document.getElementById("email").value,
        "phoneNr": document.getElementById("phoneNr").value,
        "location": document.getElementById("location").value

    };
}

function getCredentialsFromLoginContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

// Financial functions

function formatNumber(num) {
    if (!isDecimal(num)) {
        return 0;
    }
    num = Math.round(num * 100) / 100;
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function getComponentFromModal() {
    return {
        "id": document.getElementById("id").value, 
        "priceOfferDetailId": getQueryParam('priceOfferDetailsId'),
        "name": document.getElementById("name").value,
        "description": document.getElementById("description").value,
        "quantity": document.getElementById("quantity").value,
        "unit": document.getElementById("unit").value

    };
}
